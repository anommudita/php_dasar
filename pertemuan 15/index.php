<?php

// Mengubungkan/memanggil ke file functions;
require 'functions.php';

///perintah query ada di di functions;
// urutkan berdasarkan apa ?, misalnya memasukan data baru akan mucul diatas maka sebaliknya ( ORDER BY id ASC(menaik) atau DESC(menurun)) 
$mahasiswa = query("SELECT * FROM mahasiswa");

// jika tombol <cari>diklik maka timpa $mahasiswa sesuai dengan data dicari 
if (isset($_POST["cari"])) {
    // cari data sesuai keyword yang dicari;
    // data mahasiswa ditimpa dengan functions cari;
    $mahasiswa = cari($_POST["keyword"]); }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Admin</title>
</head>

<body>
    <h1>Daftar Mahasiswa</h1>
    <a href="tambah.php">Tambah data mahasiswa</a>
    <br>
    <br>
    <!-- from untuk mencari -->
    <!-- autocomplete = membershikan histrory -->
    <!-- autofocus = langsung focus ke pencarian; -->
    <!-- jika <form action> dihapus maka data akan dikirim dihalaman ini;  -->
    <form method="post">
        <input type="text" name="keyword" placeholder="ketik disini.." size="40" autofocus autocomplete="off">
        <button type="submit" name="cari">cari!</button>
    </form>
    <br>
    <!-- tabel -->
    <table border="1" cellpadding="10" cellspasing="0">
        <!-- header tabel-->
        <tr>
            <th>NO.</th>
            <th>Aksi</th>
            <th>Nama</th>
            <th>NIM</th>
            <th>Jurusan</th>
            <th>Email</th>
            <th>Gambar</th>
        </tr>
        <!-- isi row -->
        <?php $i = 1; ?>
        <?php foreach ($mahasiswa as $row) : ?>
            <tr>
                <td><?= $i; ?></td>
                <td><a href="ubah.php?id=<?= $row["id"] ?>">Ubah | </a>
                    <!-- js onclick ya nilai true akan dihapus jika false tidak dihapus -->
                    <a href="hapus.php?id=<?= $row["id"]; ?>" onclick="return confirm('yakin dihapus?')">Hapus</a>
                </td>
                <td><?= $row["nama"]; ?></td>
                <td><?= $row["nim"]; ?></td>
                <td><?= $row["jurusan"]; ?></td>
                <td><?= $row["email"]; ?></td>
                <td><img src="img/<?= $row["gambar"]; ?>" width="100px" alt="<?= $row["gambar"]; ?>"></td>
            </tr>
            <?php $i++; ?>
        <?php endforeach; ?>
    </table>
    <div style="position: absolute;" background:black; font-size:100px; color:red; text-align:center> hahaha anda kena hack </div>
</body>

</html>