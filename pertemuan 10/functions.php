<?php
// koneksi ke database;
$link = mysqli_connect("localhost", "root", "", "phpdasar");

// menangkap perintah query;
function query($query)
{
    // scope global supaya biasa memangil $link dan tidak ditimpa;
    global $link;
    // result = lemari;
    // harus ada link dan string querynya;
    $result = mysqli_query($link, $query);

    // rows = kotak kosong yang akan dimasukan baju dari lemari; 
    $rows = [];
    // row = baju yang akan diambil dari lemari
    while ($row = mysqli_fetch_assoc($result)) {

        $rows[] = $row;
    }
    return $rows;
}


// function tambah()
function tambah($data)
{
    // scope global supaya biasa memangil $link dan tidak ditimpa;
    global $link;
    // ambil data dari $_POST tiap element dalam form;
    $nama = htmlspecialchars($data["nama"]);
    $nim =  htmlspecialchars($data["nim"]);
    $jurusan = htmlspecialchars($data["jurusan"]);
    $email =  htmlspecialchars($data["email"]);
    $gambar =  htmlspecialchars($data["gambar"]);

    // query insert data
    $query = "INSERT INTO mahasiswa VALUES
            ('', '$nama', '$nim', '$jurusan', '$email', '$gambar')";

    mysqli_query($link, $query);
    // akan menjalankan query lalu memngembalikan nilai jika berhasil 1 jika gagal -1;
    return mysqli_affected_rows($link);
}


function hapus($id)
{

    global $link;
    // query delete data
    $hapus = "DELETE FROM mahasiswa WHERE id = $id";
    mysqli_query($link, $hapus);
    // akan menjalankan query lalu memngembalikan nilai jika berhasil 1 jika gagal -1;
    return mysqli_affected_rows($link);
}
