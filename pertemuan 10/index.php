<?php

// Mengubungkan/memanggil ke file functions;
require 'functions.php';

///perintah query ada di di functions;
$mahasiswa = query("SELECT * FROM mahasiswa");

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Admin</title>
</head>

<body>
    <h1>Daftar Mahasiswa</h1>
    <a href="tambah.php">Tambah data mahasiswa</a>
    <table border="1" cellpadding="10" cellspasing="0">
        <!-- header form -->
        <tr>
            <th>NO.</th>
            <th>Aksi</th>
            <th>Nama</th>
            <th>NIM</th>
            <th>Jurusan</th>
            <th>Email</th>
            <th>Gambar</th>
        </tr>
        <!-- isi row -->
        <?php $i = 1; ?>
        <?php foreach ($mahasiswa as $row) : ?>
            <tr>
                <td><?= $i; ?></td>
                <td><a href="">Ubah | </a>
                    <!-- js onclick ya nilai true akan dihapus jika false tidak dihapus -->
                    <a href="hapus.php?id=<?= $row["id"]; ?>" onclick="return confirm('yakin daihapus?')">Hapus</a>
                </td>
                <td><?= $row["nama"]; ?></td>
                <td><?= $row["nim"]; ?></td>
                <td><?= $row["jurusan"]; ?></td>
                <td><?= $row["email"]; ?></td>
                <td><img src="img/<?= $row["gambar"]; ?>" width="100px" alt="<?= $row["gambar"]; ?>"></td>
            </tr>
            <?php $i++; ?>
        <?php endforeach; ?>
    </table>
    <div style="position: absolute;" background:black; font-size:100px; color:red; text-align:center> hahaha anda kena hack </div>
</body>

</html>