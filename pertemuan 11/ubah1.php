<?php
// Mengubungkan/memanggil ke file functions tambah; 
require 'functions.php';
// koneksi ke database;
// $link = mysqli_connect("localhost", "root", "", "phpdasar");

// ambil data dari url
$id = $_GET["id"];

// query data mahasiswa berdasarkan id dan cukup sekali dielement 0;
$mhs = query("SELECT * FROM mahasiswa WHERE id = $id")[0];
var_dump($mhs);

// cek apakah tombol submit sudah ditekan atau belum 
if (isset($_POST["submit"])) {

    // jika  ditekan mengambil semua data barunya ;
    if (ubah($_POST) > 0) {

        echo "<script>
                alert('data berhasil diubah');
                document.location.href = 'index.php';
            </script>
        ";
    } else {
        echo "<script>
                alert('data gagal diubah');
                document.location.href = 'index.php';
            </script>
        ";
    }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Data Mahasiswa</title>
</head>

<body>
    <h1>Update Data Mahasiswa</h1>

    <form method="post">
        <!-- element data id yang disembunyikan dan tidak diinput oleh user; -->
        <input type="hidden" name="id" value="<?= $mhs = ["id"]; ?>">
        <ul>
            <li>
                <label for="nama">Nama :</label>
                <input type="text" name="nama" id="nama" required value="anom">
            </li>
            <li>
                <label for="nim">NIM :</label>
                <input type="text" name="nim" id="nim" value="<?= $mhs["nim"]; ?>">
            </li>
            <li>
                <label for="jurusan">Jurusan :</label>
                <input type="text" name="jurusan" id="jurusan" value="<?= $mhs["jurusan"]; ?>">
            </li>
            <li>
                <label for="email">Email :</label>
                <input type="text" name="email" id="email" value="<?= $mhs["email"]; ?>">
            </li>
            <li>
                <label for="gambar">Gambar :</label>
                <input type="text" name="gambar" id="gambar" value="<?= $mhs["gambar"]; ?>">
            </li>
            <li>
                <button type="submit" name="submit">Ubah Data!</button>
            </li>
        </ul>



    </form>

</body>

</html>