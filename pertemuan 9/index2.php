<?php

// function koneksi ke database
$link = mysqli_connect("localhost", "root", "", "phpdasar");

// Ambil data dari tabel mahasiswa / query data mahasiswa
$result = mysqli_query($link, "SELECT * FROM mahasiswa");

//Ambil data (fetch) mahasiswa dari object result
// mysqli_fetch_row() mengembalikan nilai array numerik;
// mysqli_fetch_assoc() mengembalikan nilai array associative;
// mysqli_fetch_array() mengembalikan nilai array numerik dan associative / nilai akan double ;
// mysqli_fetch_object() mengembalikan nilai berupa objetc ($mhs->nama);

// while ($mhs = mysqli_fetch_assoc($result)) {
//     var_dump($mhs["gambar"]);
// }

/// cek eror pada isi tables;
// if (!$result) {
//     echo mysqli_error($link);
// }

?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Admin</title>
</head>

<body>
    <h1>Daftar Mahasiswa</h1>
    <table border="1" cellpadding="10" cellspasing="0">
        <!-- header form -->
        <tr>
            <th>NO.</th>
            <th>Aksi</th>
            <th>Nama</th>
            <th>NIM</th>
            <th>Jurusan</th>
            <th>Email</th>
            <th>Gambar</th>
        </tr>
        <!-- isi row -->
        <?php $i = 1; ?>
        <?php while ($row = mysqli_fetch_assoc($result)) : ?>
            <tr>
                <td><?= $i; ?></td>
                <td><a href="">Ubah | </a> <a href="">Hapus</a></td>
                <td><?= $row["nama"]; ?></td>
                <td><?= $row["nim"]; ?></td>
                <td><?= $row["jurusan"]; ?></td>
                <td><?= $row["email"]; ?></td>
                <td><img src="img/<?= $row["gambar"]; ?>" width="100px" alt="<?= $row["gambar"]; ?>"></td>
            </tr>
            <?php $i++; ?>
        <?php endwhile; ?>
    </table>
</body>

</html>