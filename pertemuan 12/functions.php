<?php
// koneksi ke database;
$link = mysqli_connect("localhost", "root", "", "phpdasar");

// menangkap perintah query;
// Read()
function query($query)
{
    // scope global supaya biasa memanggil $link dan tidak ditimpa;
    global $link;
    // result = lemari;
    // harus ada link dan string querynya;
    $result = mysqli_query($link, $query);

    // rows = kotak kosong yang akan dimasukan baju dari lemari; 
    $rows = [];
    // row = baju yang akan diambil dari lemari
    while ($row = mysqli_fetch_assoc($result)) {

        $rows[] = $row;
    }
    return $rows;
}

// function insert
function tambah($data)
{
    // scope global supaya biasa memanggil $link dan tidak ditimpa;
    global $link;
    // ambil data dari $_POST tiap element dalam form;
    $nama = htmlspecialchars($data["nama"]);
    $nim = htmlspecialchars($data["nim"]);
    $jurusan = htmlspecialchars($data["jurusan"]);
    $email =  htmlspecialchars($data["email"]);
    $gambar =  htmlspecialchars($data["gambar"]);

    // query insert data
    $query = "INSERT INTO mahasiswa VALUES
            ('', '$nama', '$nim', '$jurusan', '$email', '$gambar')";

    mysqli_query($link, $query);
    // akan menjalankan query lalu memngembalikan nilai jika berhasil 1 jika gagal -1;
    return mysqli_affected_rows($link);
}

// delete
function hapus($id)
{

    global $link;
    // query delete data
    $hapus = "DELETE FROM mahasiswa WHERE id = $id";
    mysqli_query($link, $hapus);
    // akan menjalankan query lalu memngembalikan nilai jika berhasil 1 jika gagal -1;
    return mysqli_affected_rows($link);
}

// function update
function ubah($data)
{
    // scope global supaya biasa memangil $link dan tidak ditimpa;
    global $link;
    // ambil data dari $_POST tiap element dalam form;
    $id = $data["id"];
    $nama = htmlspecialchars($data["nama"]);
    $nim =  htmlspecialchars($data["nim"]);
    $jurusan = htmlspecialchars($data["jurusan"]);
    $email =  htmlspecialchars($data["email"]);
    $gambar =  htmlspecialchars($data["gambar"]);

    // query insert data
    // mengambil data lama dan ditimpa dengan data baru , jika data ada yang tidak diganti maka data lama yang akan ditimpa;
    $query = "UPDATE mahasiswa SET 
                nama = '$nama',
                nim = '$nim',
                jurusan = '$jurusan',
                email = '$email',
                gambar ='$gambar'
                WHERE id = $id
                ";
    mysqli_query($link, $query);
    // akan menjalankan query lalu memngembalikan nilai jika berhasil 1 jika gagal -1;
    return mysqli_affected_rows($link);
}


// functions cari!
function cari($keyword)
{
    // query read mencari;
    // LIKE = apapun dicari semua dari belakang dicari dan akan ditampil;
    // %keyword% = apapun depan dan belakang akan dicari;
    $query = "SELECT * FROM mahasiswa WHERE
                nama LIKE '%$keyword%' OR
                nim LIKE '%$keyword%' OR
                jurusan LIKE '%$keyword%' OR
                email LIKE '%$keyword%' 
                ";
    // memanfaat function query
    return query($query);
}
