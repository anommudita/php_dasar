<?php


// kita cek apakah user sudah berhasil login apa belum / sessionnya sudah dijalankan apa belum
// jika belum login akan ditendang di halaman login
session_start();

if (isset($_SESSION["login"])) {
    header("Location: index.php");
    exit;
}

require 'functions.php';
// apakah tombol login sudah ditekan apa belum;
if (isset($_POST["login"])) {
    // mengambil data lewat post;
    $username = $_POST["username"];
    $password = $_POST["password"];

    // pengecekan username apakah sudah terdaftar didatabase;
    $result = mysqli_query($link, "SELECT * FROM user WHERE username='$username'");


    // cek username 
    // jika hasilnya 1 maka ada usernam , dan lanjut pengecekan password
    if (mysqli_num_rows($result) === 1) {

        // cek password
        $row = mysqli_fetch_assoc($result);

        // menterjemahkan password string yang diacak;
        if (password_verify($password, $row["password"])) {

            // set dulu sessionya

            $_SESSION["login"] = true;

            header("Location: index.php");
            exit;
        }
    }
    // jika eror maka keluar dari kondisi;
    $error = true;
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Login</title>
    <style>
        label {

            display: block;
        }
    </style>
</head>

<body>
    <H1>Halaman Login </H1>
    <!-- jika ada eror maka memunculkan tulisan ini -->
    <?php if (isset($error)) : ?>
        <p style="color:red; font-style:italic">username / password yang anda masukan salah</p>

    <?php endif; ?>

    <form action="" method="POST">

        <ul>

            <li>
                <label for="username">Username: </label>
                <input type="text" name="username" id="username">
            </li>

            <li>
                <label for="password">Password :</label>
                <input type="password" name="password" id="password">
            </li>

            <li>
                <button type="sumbit" name="login">Login!</button>
            </li>

        </ul>
    </form>

</body>

</html>